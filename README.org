* Bad Moon Rising.
One of the creative coding example by deconbatch that draws Archimedes's spiral with the colors that get automatically.
[[https://www.deconbatch.com/2019/11/bad-moon-rising.html]['Bad Moon Rising.' on Creative Coding : Examples with Code.]]
** Description
A creative coding art work made with Processing on Java programming language.

I used colormind color picker API again just like [[https://deconbatch.blogspot.com/2019/11/big-ten-inch-record.html]['Big Ten Inch Record.']]

Colormind is a color scheme generator that uses deep learning.
http://colormind.io/

This time I made weird striped pattern animation with Perlin noise.

This code does not display any images on the screen but generates image files in frames directory. 
You can make an animation with these files.

** Change log
   - created : 2019/11/07

